#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

# Create group/user (password is sha-512 salted with random data)
groupadd -g 1001 acasscan
adduser -u 1001 -g 1001 acasscan -c 'ACAS Scan' -p 'x8g14wGdpGAHI'

# Sudo permissions
echo 'acasscan ALL=(ALL) NOPASSWD: ALL' | sudo tee /etc/sudoers.d/acasscan
chmod 440 /etc/sudoers.d/acasscan

# Allow password auth
cat << EOF >> /etc/ssh/sshd_config

# ACAS Scan
Match User acasscan
        PasswordAuthentication yes
        X11Forwarding no
        AllowTCPForwarding no

EOF
chmod 600 /etc/ssh/sshd_config
systemctl restart sshd

# Prevent cloud-init ssh module changes
sed -i 's/ssh_pwauth:   [0-9]\+/ssh_pwauth:   unchanged/g' /etc/cloud/cloud.cfg


