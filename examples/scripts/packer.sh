#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

packer_version="1.6.6"

# https://github.com/cracklib/cracklib/issues/7
if test -f "/etc/redhat-release"; then
    echo "Unlinking cracklib deprecated packer binary."
    unlink /usr/sbin/packer
fi

# Packer
wget --quiet https://releases.hashicorp.com/packer/"${packer_version}"/packer_"${packer_version}"_linux_amd64.zip
unzip -qq packer_"${packer_version}"_linux_amd64.zip
sudo mv packer /usr/bin/
sudo chown root:root /usr/bin/packer
sudo chmod 555 /usr/bin/packer
rm -f packer_"${packer_version}"_linux_amd64.zip

# https://github.com/bonclay7/aws-amicleaner
#pip3 install --quiet aws-amicleaner
