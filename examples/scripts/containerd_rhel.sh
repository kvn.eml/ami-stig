#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

rhel_version=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*// | awk -F. '{ print $1 }'`

# yum list containerd.io --showduplicates | sort -r
containerd_version="1.4.3"

# Required
rpm --import https://www.centos.org/keys/RPM-GPG-KEY-CentOS-${rhel_version}
yum install -y -q http://mirror.centos.org/centos/${rhel_version}/extras/x86_64/Packages/container-selinux-2.107-3.el${rhel_version}.noarch.rpm

# Containerd
yum-config-manager --enable docker-ce-stable
yum install -y -q containerd.io-${containerd_version}
yum versionlock containerd.io

# Enable
systemctl enable containerd