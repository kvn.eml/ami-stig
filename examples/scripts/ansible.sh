#!/bin/bash

#install EPEL repo
sudo dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

#install ansible
sudo dnf -y install ansible

#install RedHatOfficial.rhel8_stig Ansible Role
sudo ansible-galaxy install RedHatOfficial.rhel8_stig --ignore-cert

#Create playbook.yml
echo "- hosts: all" >> playbook.yml
echo "  roles:" >> playbook.yml
echo "    - { role: RedHatOfficial.rhel8_stig }" >> playbook.yml

#Apply Ansible playbook
sudo ansible-playbook -i "localhost," -c local playbook.yml


