#!/usr/bin/env bash

# set -o errexit
# set -o pipefail
# set -o xtrace

REGION=`ec2metadata --availability-zone | sed 's/.$//'`
ID=`ec2metadata --instance-id`
PATCHED=`date +"%Y-%m-%d"`

aws --region $REGION ec2 create-tags --resources $ID --tags Key=Patched,Value=$PATCHED
