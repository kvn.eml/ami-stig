#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

FILE="conf/chrony.conf"

# Check for local chrony.conf
if test -f "$FILE"; then
  echo "$FILE exists."
  cp -f $FILE /etc/chrony.conf
  chown root:root /etc/chrony.conf
  chmod 644 /etc/chrony.conf
  systemctl restart chronyd
fi

rm -rf conf

ip_netblock=`hostname -I | cut -f1 -d.`

# Check network
if [ ${ip_netblock} -eq 214 ]; then
systemctl disable firewalld
cat << EOF > /etc/cloud/cloud.cfg.d/10_systemctl.cfg
runcmd:
  - [ systemctl, disable, amazon-cloudwatch-agent ] 
EOF
chmod 644 /etc/cloud/cloud.cfg.d/10_systemctl.cfg
  exit 0
fi

SPLUNK_HOME="/opt/splunkforwarder/bin"
MCAFEE_HOME="/opt/McAfee/agent/bin"

echo "Disabling agents for Dev/Test"

# EnCase
systemctl disable encase
systemctl stop encase

# Splunk
$SPLUNK_HOME/splunk disable boot-start -user splunk
# sudo -H -u splunk $SPLUNK_HOME/splunk stop

# Tanium
systemctl disable taniumclient
systemctl stop taniumclient

# McAfee
systemctl disable cma
systemctl stop cma

systemctl disable mcafee.ma
systemctl stop mcafee.ma

$MCAFEE_HOME/maconfig -stop
# chmod 644 /etc/systemd/system/mcafee.ma.service
# chattr +i /etc/systemd/system/mcafee.ma.service

# firewalld
systemctl disable firewalld

# https://dustymabe.com/2015/08/03/installingstarting-systemd-services-using-cloud-init/
cat << EOF > /etc/cloud/cloud.cfg.d/10_systemctl.cfg
runcmd:
  - [ systemctl, disable, amazon-cloudwatch-agent ]
  - [ systemctl, disable, cma.service ] 
  - [ systemctl, disable, mcafee.ma ]
EOF
chmod 644 /etc/cloud/cloud.cfg.d/10_systemctl.cfg

# wget
rm -f /root/.wgetrc
rm -f /home/ec2-user/.wgetrc