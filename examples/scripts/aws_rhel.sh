#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

# Check for argument
if [ $# -eq 0 ]; then
    echo "No argument provided"
    exit 1
fi

# Map input variables
case $1 in
   us-east-1 | us-west-2)
   region=$1
   ;;
   us-gov-west-1 | us-gov-east-1)
   region=$1
   ;;
  *)
   echo "Unknown option: $1"
   exit 1
   ;;
esac

echo "Installing AWS Agents using ${region}"

# AWS CLI
# pip3 install --user --quiet awscli
# pip3 install --user --quiet boto3

# or

curl -s "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip -qq awscliv2.zip
sudo ./aws/install --bin-dir /usr/bin
sudo rm -rf aws
sudo rm -f awscliv2.zip

# .aws directory
mkdir -p ~/.aws
cat << EOF > ~/.aws/config
[default]
region = ${region}
output = json
#ca_bundle = /etc/pki/ca-trust/source/anchors/all-intermediates.cer
s3 =
    signature_version = s3v4
EOF
chmod 600 ~/.aws/config

# SSM
sudo yum install -y -q https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
sudo systemctl enable amazon-ssm-agent
sudo systemctl start amazon-ssm-agent
#rm -f amazon-ssm-agent.rpm

# CloudWatch
sudo yum install -y -q https://s3.amazonaws.com/amazoncloudwatch-agent/redhat/amd64/latest/amazon-cloudwatch-agent.rpm
sudo systemctl disable amazon-cloudwatch-agent
sudo systemctl stop amazon-cloudwatch-agent
#rm -f amazon-cloudwatch-agent.rpm

# OS Check
# if [ ${rhel_version} -eq 8 ]; then
#     echo "Skipping Inspector for RHEL ${rhel_version}"
#     exit 0
# fi

# # Inspector
wget --quiet https://inspector-agent.amazonaws.com/linux/latest/install
sudo bash install
#sudo bash install -u false
sudo systemctl disable awsagent
sudo systemctl stop awsagent
rm -f install
