#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

rpm -U --quiet packages/encase-*x86_64.rpm
rm -f packages/encase-*x86_64.rpm
systemctl enable encase