#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

MCAFEE_HOME="/opt/McAfee/agent/bin"

# Create group/user
groupadd -g 1003 mfe
adduser -u 1003 -g 1003 mfe -c 'McAfee Agent'

# Install
cd packages
unzip -qq McAfeeAgent*_Linux.zip
sh install.sh -i

# Cleanup
rm -f packages/McAfeeAgent*_Linux.zip
cd ../
rm -rf packages

# Reconfigure GUID
systemctl stop cma
systemctl stop mcafee.ma
$MCAFEE_HOME/maconfig -enforce -noguid

# Set HOME
cat << EOF > /etc/profile.d/mcafee.sh
MCAFEE_HOME=/opt/McAfee/agent/bin/
export PATH=$PATH:/opt/McAfee/agent/bin/
EOF
chmod 644 /etc/profile.d/mcafee.sh
chown root:root /etc/profile.d/mcafee.sh