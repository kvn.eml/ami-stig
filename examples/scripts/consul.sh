#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

consul_version="1.9.0"

# Consul
wget --quiet https://releases.hashicorp.com/consul/"${consul_version}"/consul_"${consul_version}"_linux_amd64.zip
unzip -qq consul_"${consul_version}"_linux_amd64.zip
sudo mv consul /usr/bin/
sudo chown root:root /usr/bin/consul
sudo chmod 555 /usr/bin/consul
rm -f consul_"${consul_version}"_linux_amd64.zip
