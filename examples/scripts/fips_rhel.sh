#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

# rhel_version=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*// | awk -F. '{ print $1 }'`

# Ignore exit 100 return code
# https://access.redhat.com/solutions/2779441
yum check-update ignore_retcode=true -q

# Upgrade kernel before reboot
yum upgrade -y -q kernel

# https://access.redhat.com/solutions/137833
# FIPS enable
yum install -y -q dracut-fips dracut-fips-aesni
mv -v /boot/initramfs-$(uname -r).img{,.bak}
dracut

sed -i -e "s/GRUB_CMDLINE_LINUX=\"\(.*\)\"/GRUB_CMDLINE_LINUX=\"\1 fips=1\"/" /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg
