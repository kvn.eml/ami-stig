#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

yum install -y -q nvme-cli

# See: https://github.com/transferwise/ansible-ebs-automatic-nvme-mapping
# See: https://github.com/leboncoin/terraform-aws-nvme-example

for blkdev in $(nvme list | awk '/^\/dev/ { print $1 }') ; do
  mapping=$(nvme id-ctrl --raw-binary "${blkdev}" | cut -c3073-3104 | tr -s ' ' | sed 's/ $//g' | grep -Po '(sd[a-z]|xvd[a-z])')
  ln -s "${blkdev}" "/dev/${mapping}"
done
