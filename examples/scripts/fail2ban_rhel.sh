#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

rhel_version=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*// | awk -F. '{ print $1 }'`

# Install
yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/f/fail2ban-server-0.11.1-10.el${rhel_version}.noarch.rpm
yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/f/fail2ban-sendmail-0.11.1-10.el${rhel_version}.noarch.rpm
yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/f/fail2ban-firewalld-0.11.1-10.el${rhel_version}.noarch.rpm
yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/f/fail2ban-0.11.1-10.el${rhel_version}.noarch.rpm
yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/f/fail2ban-systemd-0.11.1-10.el${rhel_version}.noarch.rpm

# Conf
#cp -pf /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
#sed -i 's/#ignoreip =/ignoreip =/g' /etc/fail2ban/jail.local
cat << EOF > /etc/fail2ban/jail.local
[DEFAULT]
# Ban hosts for X time:
bantime = 30m

# When a client should be banned
findtime = 10m
maxretry = 5

ignoreip = 127.0.0.1/8 204.115.176.0/21

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled = true
EOF
chmod 644 /etc/fail2ban/jail.local
chown root:root /etc/fail2ban/jail.local

# Enable
systemctl enable fail2ban
