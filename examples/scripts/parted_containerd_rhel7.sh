#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

OS_FS="xfs"

# Parted
yum install -y -q parted

# Partition disk
parted -s /dev/nvme0n1 mkpart var 15GB 27GB
parted -s /dev/nvme0n1 mkpart log 27GB 32GB
parted -s /dev/nvme0n1 mkpart audit 32GB 40GB
parted -s /dev/nvme0n1 mkpart vtmp 40GB 42GB
parted -s /dev/nvme0n1 mkpart tmp 42GB 45GB
parted -s /dev/nvme0n1 mkpart home 45GB 53GB
parted -s /dev/nvme0n1 mkpart opt 53GB 100%
partprobe
growpart /dev/nvme0n1 2

# Grow /
xfs_growfs /

# XFS/EXT4 file systems
mkfs.${OS_FS} -q /dev/nvme0n1p3
mkfs.${OS_FS} -q /dev/nvme0n1p4
mkfs.${OS_FS} -q /dev/nvme0n1p5
mkfs.${OS_FS} -q /dev/nvme0n1p6
mkfs.${OS_FS} -q /dev/nvme0n1p7
mkfs.${OS_FS} -q /dev/nvme0n1p8
mkfs.${OS_FS} -q /dev/nvme0n1p9

# Docker
# https://docs.docker.com/storage/storagedriver/select-storage-driver/#supported-backing-filesystems
mkfs.xfs -f -n ftype=1 /dev/sdf
# or 
#mkfs.ext4 /dev/sdf

# Staging Directory
mkdir -p /mnt_tmp
mkdir -p /mnt_tmp/var
mkdir -p /mnt_tmp/tmp
mkdir -p /mnt_tmp/home
mkdir -p /mnt_tmp/opt

# Copy files over to stage
# rsync to maintain acl and links
rsync -aHAXxS /tmp/ /mnt_tmp/tmp/
rsync -aHAXxS /var/ /mnt_tmp/var/
rsync -aHAXxS /home/ /mnt_tmp/home/
rsync -aHAXxS /opt/ /mnt_tmp/opt/

# Move current
systemctl stop chronyd
mv /tmp /tmp_orig
#systemctl stop var-lib-nfs-rpc_pipefs.mount
mv /var /var_orig
systemctl stop NetworkManager
mv /home /home_orig
mv /opt /opt_orig

# Mount partitions and copy data from stage to target
mkdir /var
mount -t ${OS_FS} /dev/nvme0n1p3 /var
mkdir -p /var/lib/containerd
mkdir /var/log
mount -t ${OS_FS} /dev/nvme0n1p4 /var/log
mkdir /var/log/audit
mount -t ${OS_FS} /dev/nvme0n1p5 /var/log/audit
mkdir /var/tmp
mount -t ${OS_FS} /dev/nvme0n1p6 /var/tmp
rsync -aHAXxS /mnt_tmp/var/ /var/

mkdir /tmp
mount -t ${OS_FS} /dev/nvme0n1p7 /tmp
mkdir /home
mount -t ${OS_FS} /dev/nvme0n1p8 /home
mkdir /opt
mount -t ${OS_FS} /dev/nvme0n1p9 /opt

rsync -aHAXxS /mnt_tmp/tmp/ /tmp/
rsync -aHAXxS /mnt_tmp/home/ /home/
rsync -aHAXxS /mnt_tmp/opt/ /opt/

# Set up mountpoints in fstab
#cp /etc/fstab /etc/fstab.orig

ROOT_UUID=$(blkid -o value -s UUID /dev/nvme0n1p2)
VAR_UUID=$(blkid -o value -s UUID /dev/nvme0n1p3)
LOG_UUID=$(blkid -o value -s UUID /dev/nvme0n1p4)
AUDIT_UUID=$(blkid -o value -s UUID /dev/nvme0n1p5)
VTMP_UUID=$(blkid -o value -s UUID /dev/nvme0n1p6)
TMP_UUID=$(blkid -o value -s UUID /dev/nvme0n1p7)
HOME_UUID=$(blkid -o value -s UUID /dev/nvme0n1p8)
OPT_UUID=$(blkid -o value -s UUID /dev/nvme0n1p9)
CONTAINERD_UUID=$(blkid -o value -s UUID /dev/sdf)

# /tmp noexec removed for terraform
bash -c "cat << EOF > /etc/fstab
UUID=${ROOT_UUID} /                       ${OS_FS}     defaults        0 0
UUID=${VAR_UUID} /var                    ${OS_FS}     defaults        0 0
UUID=${LOG_UUID} /var/log                ${OS_FS}     defaults        0 0
UUID=${AUDIT_UUID} /var/log/audit          ${OS_FS}     defaults        0 0
UUID=${VTMP_UUID} /var/tmp                ${OS_FS}     rw,nodev,noexec,relatime,seclabel,attr2,inode64,noquota,nosuid        0 0
UUID=${TMP_UUID} /tmp                    ${OS_FS}     rw,nodev,relatime,seclabel,attr2,inode64,noquota,nosuid        0 0
UUID=${HOME_UUID} /home                   ${OS_FS}     rw,nodev,relatime,seclabel,attr2,inode64,noquota,nosuid        0 0
UUID=${OPT_UUID} /opt                    ${OS_FS}     defaults        0 0
UUID=${CONTAINERD_UUID} /var/lib/containerd     ${OS_FS}     defaults        0 0
EOF"

# Cleanup stage
rm -fR /mnt_tmp
rm -rf /tmp_orig
rm -rf /var_orig
rm -rf /home_orig
rm -rf /opt_orig

# Reboot
reboot