#https://static.open-scap.org/openscap-1.2/oscap_user_manual.html#_practical_examples

TS="$(date +%Y-%m-%d_%H-%M-%S)"
artifacts="/root/stig/artifacts"
mkdir -p $artifacts
sudo dnf -y install openscap-scanner scap-security-guide

oscap info /usr/share/xml/scap/ssg/content/ssg-rhel8-ds-1.2.xml
oscap xccdf eval --profile xccdf_org.ssgproject.content_profile_stig \
  --fetch-remote-resources \
  --results $artifacts/ssg-rhel8-ds-1.2-result.xml \
  --report $artifacts/ssg-rhel8-ds-1.2-report.html \
  --stig-viewer $artifacts/ssg-rhel8-oscap-compliance-stig.xml \
  /usr/share/xml/scap/ssg/content/ssg-rhel8-ds-1.2.xml

cp $artifacts/ssg-rhel8* /tmp/
tar -cvzf /root/stig-$TS.tgz $artifacts || true
chown ec2-user.ec2-user /tmp/ssg-rhel8*

