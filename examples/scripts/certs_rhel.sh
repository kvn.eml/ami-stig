#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

FILE="certs/all-intermediates.p7b"

# mkdir certs

# Check for p7b file
if test -f "$FILE"; then
  echo "$FILE exists."
  openssl pkcs7 -inform DER -outform PEM -in $FILE -print_certs > certs/all-intermediates.cer
fi

# Copy local certs to destination
mv certs/*.{cer,crt} /etc/pki/ca-trust/source/anchors/
chown root:root /etc/pki/ca-trust/source/anchors/*.{cer,crt}
update-ca-trust extract
rm -rf certs

# wget
cat << EOF > ~/.wgetrc
ca_directory = /etc/pki/ca-trust/source/anchors
EOF
chmod 640 ~/.wgetrc

cp ~/.wgetrc /home/ec2-user/
chown ec2-user:ec2-user /home/ec2-user/.wgetrc

# curl
cat << EOF > ~/.curlrc
capath = /etc/pki/ca-trust/source/anchors
EOF
chmod 640 ~/.curlrc

cp ~/.curlrc /home/ec2-user/
chown ec2-user:ec2-user /home/ec2-user/.curlrc
