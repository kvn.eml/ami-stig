#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

# Copy local repos to destination
mv repos/* /etc/yum.repos.d/
chown root:root /etc/yum.repos.d/*.repo
chmod 644 /etc/yum.repos.d/*.repo
rm -rf repos