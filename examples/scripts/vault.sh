#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

vault_version="1.6.1"

# Vault
wget --quiet https://releases.hashicorp.com/vault/"${vault_version}"/vault_"${vault_version}"_linux_amd64.zip
unzip -qq vault_"${vault_version}"_linux_amd64.zip
sudo mv vault /usr/bin/
sudo chown root:root /usr/bin/vault
sudo chmod 555 /usr/bin/vault
rm -f vault_"${vault_version}"_linux_amd64.zip
