#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

# Install
yum install -y -q yum-cron

# https://access.redhat.com/solutions/2823901
sed -i 's/update_cmd = default/update_cmd = security/g' /etc/yum/yum-cron.conf
sed -i 's/update_messages = yes/update_messages = no/g' /etc/yum/yum-cron.conf
sed -i 's/apply_updates = no/apply_updates = yes/g' /etc/yum/yum-cron.conf

# Enable
systemctl enable yum-cron
