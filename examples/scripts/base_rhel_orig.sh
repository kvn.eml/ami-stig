#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

rhel_version=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*// | awk -F. '{ print $1 }'`
ansible_version="2.9.15"

# Enable extra repos
# yum-config-manager --enable rhui-rhel-${rhel_version}-server-rhui-extras-rpms \
# rhui-rhel-${rhel_version}-server-rhui-optional-rpms

# https://access.redhat.com/solutions/3141231
# yum install -y -q deltarpm
yum update -y -q
yum install -y -q aide bash-completion firewalld fontconfig git lsof \
python3-pip screen tcp_wrappers unzip vim wget yum-plugin-versionlock
# redhat-lsb-core

# Remove old kernels
package-cleanup -y -q --oldkernels --count=1

# firewalld
systemctl start firewalld
#firewall-cmd --permanent --add-service=ssh
#firewall-cmd --set-default-zone=drop

# Smart Card for MFA
yum install -y -q esc pam_pkcs11

# ansible
rpm --import https://releases.ansible.com/keys/RPM-GPG-KEY-ansible-release.pub

if [ ${rhel_version} -eq 7 ]; then
  rpm --import http://download.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-${rhel_version}
  yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/s/sshpass-1.06-1.el${rhel_version}.x86_64.rpm
  yum install -y -q https://releases.ansible.com/ansible/rpm/release/epel-${rhel_version}-x86_64/ansible-${ansible_version}-1.el${rhel_version}.ans.noarch.rpm
fi

if [ ${rhel_version} -eq 8 ]; then
  rpm --import http://download.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-${rhel_version}
  yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/Everything/x86_64/Packages/s/sshpass-1.06-9.el${rhel_version}.x86_64.rpm
  # yum install -y -q https://releases.ansible.com/ansible/rpm/release/epel-${rhel_version}-x86_64/ansible-${ansible_version}-1.el${rhel_version}.ans.noarch.rpm
fi

# or
# yum install -y -q https://dl.fedoraproject.org/pub/epel/epel-release-latest-${rhel_version}.noarch.rpm
# yum update -y -q
# yum install -y -q ansible


# jq
if [ ${rhel_version} -eq 7 ]; then
  yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/o/oniguruma-6.8.2-1.el${rhel_version}.x86_64.rpm
  yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/j/jq-1.6-2.el${rhel_version}.x86_64.rpm
fi

# if [ ${rhel_version} -eq 8 ]; then
#  yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/o/oniguruma-6.8.2-1.el${rhel_version}.x86_64.rpm
#  yum install -y -q https://dl.fedoraproject.org/pub/epel/${rhel_version}/x86_64/Packages/j/jq-1.6-2.el${rhel_version}.x86_64.rpm
# fi

# SSH
#  sed -i 's/#UseDNS yes/UseDNS no/g' /etc/ssh/sshd_config
#  systemctl restart sshd

# Disable services
systemctl disable rhnsd.service
systemctl disable kdump.service

# IPv6 disable
sed -i -e "s/GRUB_CMDLINE_LINUX=\"\(.*\)\"/GRUB_CMDLINE_LINUX=\"\1 ipv6.disable=1\"/" /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg

# NVMe mapping
yum install -y -q nvme-cli
mv /home/ec2-user/scripts/ebs_alias.sh /usr/local/bin/
chmod 774 /usr/local/bin/ebs_alias.sh
chown root:root /usr/local/bin/ebs_alias.sh
#/usr/local/bin/ebs_alias.sh > /dev/null 2>&1

# Patched
mv /home/ec2-user/scripts/patched_tag.sh /usr/local/bin/
chmod 774 /usr/local/bin/patched_tag.sh
chown root:root /usr/local/bin/patched_tag.sh
