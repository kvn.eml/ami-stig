#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

rhel_version=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*// | awk -F. '{ print $1 }'`

# yum list docker-ce --showduplicates | sort -r
docker_version="19.03.14"
containerd_version="1.4.3"

# Dependencies
yum install -y -q lvm2 device-mapper device-mapper-persistent-data \
device-mapper-event device-mapper-libs device-mapper-event-libs

# Required
rpm --import https://www.centos.org/keys/RPM-GPG-KEY-CentOS-${rhel_version}
yum install -y -q http://mirror.centos.org/centos/${rhel_version}/extras/x86_64/Packages/container-selinux-2.107-3.el${rhel_version}.noarch.rpm
yum install -y -q http://mirror.centos.org/centos/${rhel_version}/extras/x86_64/Packages/slirp4netns-0.3.0-1.el${rhel_version}.x86_64.rpm
yum install -y -q http://mirror.centos.org/centos/${rhel_version}/extras/x86_64/Packages/fuse3-libs-3.6.1-4.el${rhel_version}.x86_64.rpm
yum install -y -q http://mirror.centos.org/centos/${rhel_version}/extras/x86_64/Packages/fuse-overlayfs-0.7.2-6.el${rhel_version}_8.x86_64.rpm

# Docker
yum-config-manager --enable docker-ce-stable
yum install -y -q docker-ce-${docker_version} docker-ce-cli-${docker_version} containerd.io-${containerd_version}
yum versionlock docker-ce docker-ce-cli containerd.io

# Daemon
mkdir /etc/docker
cat << EOF > /etc/docker/daemon.json
{
    "live-restore": true,
    "group": "docker"
}
EOF
#chmod 600 /etc/docker/daemon.json

# Permissions
usermod -a -G docker ec2-user
#usermod -a -G dockerroot ec2-user

# Enable
systemctl enable docker