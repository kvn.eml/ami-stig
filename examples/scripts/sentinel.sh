#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

sentinel_version="0.16.1"

# Sentinel
wget --quiet https://releases.hashicorp.com/sentinel/"${sentinel_version}"/sentinel_"${sentinel_version}"_linux_amd64.zip
unzip -qq sentinel_"${sentinel_version}"_linux_amd64.zip
sudo mv sentinel /usr/bin/
sudo chown root:root /usr/bin/sentinel
sudo chmod 555 /usr/bin/sentinel
rm -f sentinel_"${sentinel_version}"_linux_amd64.zip
