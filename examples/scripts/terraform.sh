#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

terraform_version="0.13.5"

# Terraform
wget --quiet https://releases.hashicorp.com/terraform/"${terraform_version}"/terraform_"${terraform_version}"_linux_amd64.zip
unzip -qq terraform_"${terraform_version}"_linux_amd64.zip
sudo mv terraform /usr/bin/
sudo chown root:root /usr/bin/terraform
sudo chmod 555 /usr/bin/terraform
rm -f terraform_"${terraform_version}"_linux_amd64.zip
