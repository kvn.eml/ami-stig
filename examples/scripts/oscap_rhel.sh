#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

rhel_version=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*// | awk -F. '{ print $1 }'`

# Find profiles
# oscap info /usr/share/xml/scap/ssg/content/ssg-rhel${rhel_version}-xccdf.xml
#oscap_profile="stig-rhel${rhel_version}-disa" # or "C2S" or "pci-dss"
#oscap_profile=$1

# Check for argument
if [ $# -eq 0 ]; then
    echo "No argument provided"
    exit 1
fi

# Map input variables
case $1 in
  cis | CIS)
   oscap_profile="C2S"
   ;;
  disa | DISA | stig | STIG)
   oscap_profile="stig"
   #oscap_profile="stig-rhel${rhel_version}-disa"
   ;;
  pci | PCI)
   oscap_profile="pci-dss"
   ;;
  *)
   echo "Unknown profile: $1"
   exit 1
   ;;
esac

# OpenSCAP Tools
if [ ${rhel_version} -eq 8 ]; then
    sudo yum install -y -q openscap-scanner openscap-utils scap-security-guide
    exit 0
else
    sudo yum install -y -q openscap-scanner scap-security-guide
fi

# Generate ansible playbook
oscap xccdf generate fix --fix-type ansible \
--fetch-remote-resources \
--profile xccdf_org.ssgproject.content_profile_${oscap_profile} \
--output ${oscap_profile}-role.yml \
/usr/share/xml/scap/ssg/content/ssg-rhel${rhel_version}-ds.xml

# Fix file permissions
chmod 644 ${oscap_profile}-role.yml

# Remediate - Ansible
ansible-playbook -b --connection=local -i '127.0.0.1,' \
--skip-tags 'mount_option_tmp_nodev,mount_option_var_tmp_nodev,mount_option_home_nodev,mount_option_dev_shm_nodev,service_firewalld_enabled,grub2_enable_fips_mode,sudo_remove_nopasswd' \
${oscap_profile}-role.yml

# additional tags
# DISA-STIG-RHEL-07-010010,package_aide_installed,aide_periodic_cron_checking,security_patches_up_to_date,sudo_remove_nopasswd

# Remediate - Built-in (not advised)
# oscap xccdf eval --fetch-remote-resources --remediate \
# --profile xccdf_org.ssgproject.content_profile_${oscap_profile} \
# --results scan-xccdf-results.xml \
# /usr/share/xml/scap/ssg/content/ssg-rhel${rhel_version}-ds.xml

# Validate findings
# oscap xccdf eval --fetch-remote-resources \
# --profile xccdf_org.ssgproject.content_profile_${oscap_profile} \
# --results post-${oscap_profile}.xml \
# --cpe /usr/share/xml/scap/ssg/content/ssg-rhel${rhel_version}-cpe-dictionary.xml \
# --report post-${oscap_profile}.html \
# /usr/share/xml/scap/ssg/content/ssg-rhel${rhel_version}-ds.xml

# Cleanup
rm -f ${oscap_profile}-role.yml