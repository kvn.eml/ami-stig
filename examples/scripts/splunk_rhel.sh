#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

SPLUNK_HOME="/opt/splunkforwarder/bin"

groupadd -g 1002 splunk
adduser -u 1002 -g 1002 splunk -c 'Splunk Server' -d /opt/splunkforwarder

rpm -U --quiet packages/splunkforwarder-*x86_64.rpm
rm -f packages/splunkforwarder-*x86_64.rpm

# Enable FIPS
cat << EOF >> /opt/splunkforwarder/etc/splunk-launch.conf

# FIPS mode
SPLUNK_FIPS=1
EOF
chmod 644 /opt/splunkforwarder/etc/splunk-launch.conf

# Ownership
chown -R splunk:splunk $SPLUNK_HOME

# Config
sudo -H -u splunk $SPLUNK_HOME/splunk start --accept-license \
--answer-yes --no-prompt --gen-and-print-passwd

# Enable
$SPLUNK_HOME/splunk enable boot-start -user splunk

# Stop
sudo -H -u splunk $SPLUNK_HOME/splunk stop

# Clear config
sudo -H -u splunk $SPLUNK_HOME/splunk clone-prep-clear-config

# Set HOME
cat << EOF > /etc/profile.d/splunk.sh
SPLUNK_HOME=/opt/splunkforwarder/bin
export PATH=$PATH:/opt/splunkforwarder/bin
EOF
chmod 644 /etc/profile.d/splunk.sh
chown root:root /etc/profile.d/splunk.sh