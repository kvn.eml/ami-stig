#cloud-config
resize_rootfs: false # disable root resize

growpart:
  mode: false
  devices: ['/']
  ignore_growroot_disabled: false

runcmd:
  - sgdisk -e /dev/nvme0n1
  