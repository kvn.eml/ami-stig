#!/usr/bin/env bash

set -o errexit
set -o pipefail
# set -o xtrace

# yum list kubectl --showduplicates | sort -r
kubernetes_version="1.18.13"

# SSH
sed -i 's/#AllowTcpForwarding yes/AllowTcpForwarding yes/g' /etc/ssh/sshd_config
systemctl restart sshd

# Kernel modules
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
modprobe overlay
modprobe br_netfilter
modprobe ip6_udp_tunnel
modprobe ip_set
modprobe ip_set_hash_ip
modprobe ip_set_hash_net
modprobe iptable_mangle
modprobe iptable_raw
modprobe nf_conntrack_netlink
modprobe nfnetlink
modprobe veth
modprobe vxlan
modprobe x_tables
modprobe xt_comment
modprobe xt_mark
modprobe xt_multiport
modprobe xt_nat
modprobe xt_recent
modprobe xt_set
modprobe xt_statistic
modprobe xt_tcpudp
EOF

# Bridge traffic
# https://access.redhat.com/solutions/46529
modprobe br_netfilter

# IP Forwarding
cat <<EOF | sudo tee /etc/sysctl.d/100-k8s.conf
net.ipv4.ip_forward = 1
EOF
sysctl -p /etc/sysctl.d/100-k8s.conf

# Check
sysctl net.bridge.bridge-nf-call-iptables
sysctl net.bridge.bridge-nf-call-arptables
sysctl net.ipv4.ip_forward

# IPSec
yum install -y -q libreswan

# RKE2
yum-config-manager --enable rancher-rke2-common-stable rancher-rke2-1.18-stable

# Let rke2 load the rest
exit 0

# Kubernetes
yum-config-manager --enable kubernetes
yum install -y -q kubelet-${kubernetes_version} kubeadm-${kubernetes_version} kubectl-${kubernetes_version}
yum versionlock kubelet kubeadm kubectl
kubectl completion bash >/etc/bash_completion.d/kubectl

# Kubelet
systemctl enable kubelet