#!/bin/bash
set -o errexit
set -o pipefail

source $HOME/.aws/env.sh
export data_volume_size=64
export instance_type=t3.small
export root_volume_size=32
export AWS_SUBNET_ID=subnet-083ecc1532347db82
export AWS_KEYPAIR="packer"
export AWS_PRIVATE_KEY="~/.ssh/packer.pem"
packer -version > scripts/build-scripts.log
cat ./build-ami.sh >> scripts/build-scripts.log
cat ./rhel-8.json >> scripts/build-scripts.log

#	packer build  \
	packer build  -on-error=ask \
               --var aws_region=$AWS_DEFAULT_REGION \
               --var data_volume_size=$data_volume_size \
               --var instance_type=$instance_type \
               --var root_volume_size=$root_volume_size \
               --var ssh_keypair_name=$AWS_KEYPAIR \
               --var ssh_private_key_file="$AWS_PRIVATE_KEY" \
               --var subnet_id=$AWS_SUBNET_ID \
               --var vpc_id=$AWS_VPC_ID \
               ./rhel-8.json | tee ./build.log && cat ./rhel-8.json >> ./build.log && \
echo "AMI created ->  $( cat ./build.log | grep us-gov-west-1 | cut -d' ' -f2 )" 

