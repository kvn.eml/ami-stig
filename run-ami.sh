#!/bin/bash
set -o errexit
set -o pipefail

source $HOME/.aws/env.sh
export AWS_AMI_ID=$( cat build.log | grep us-gov-west-1 | cut -d' ' -f2 ) 
echo "AWS_AMI_ID = $AWS_AMI_ID" 
export AWS_KEYPAIR=""
export AWS_SECURITY_GROUP=""
export AWS_SUBNET_ID=""
export AWS_PAGER=""
export instance_type=$(cat rhel-8.json | jq -r .variables.instance_type)

docker run \
	  -it \
	  --rm \
	  -e AWS_AMI_ID \
	  -e AWS_PAGER \
	  -e AWS_DEFAULT_REGION \
	  -e AWS_SECRET_ACCESS_KEY \
	  -e AWS_ACCESS_KEY_ID \
	  -e AWS_SUBNET_ID \
	  -e AWS_SECURITY_GROUP \
	  -e AWS_KEYPAIR \
	  -e instance_type \
	  gitlab.health.mil:5050/kevin.hansen/packer/aws-cli:2.2.40 ec2 run-instances --image-id $AWS_AMI_ID --count 1 --instance-type $instance_type --key-name $AWS_KEYPAIR --security-group-ids $AWS_SECURITY_GROUP --subnet-id $AWS_SUBNET_ID | tee ./run-ami.log

export ip=$(cat ./run-ami.log | jq .Instances[].PrivateIpAddress | sed 's/"//g' )
echo "IP address = $ip"

