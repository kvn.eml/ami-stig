#!/usr/bin/env bash

set -o errexit
set -o pipefail

# Generate stig results
oscap xccdf eval --fetch-remote-resources \
  --profile xccdf_org.ssgproject.content_profile_stig \
  --results post-stig.xml \
  --cpe /usr/share/xml/scap/ssg/content/ssg-rhel8-cpe-dictionary.xml \
  --report post-stig.html \
  /usr/share/xml/scap/ssg/content/ssg-rhel8-ds.xml
