#!/usr/bin/env bash

set -o pipefail
set -o nounset
set -o errexit

source /home/ec2-user/scripts/functions.sh

# wait for cloud-init to finish
wait_for_cloudinit

# disable firewalld
systemctl disable firewalld && systemctl stop firewalld

# enable audit log
systemctl enable auditd && systemctl start auditd

# partition disks
[ $(systemctl show -p SubState --value tuned) == 'dead' ] || systemctl stop tuned
systemctl stop rsyslog crond irqbalance polkit chronyd NetworkManager
partition_disks /dev/nvme1n1

reboot
