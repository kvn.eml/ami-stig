#!/bin/bash
set -o errexit
set -o pipefail

source $HOME/.aws/env.sh

docker run \
	  -it \
	  --rm \
	  -e AWS_DEFAULT_REGION \
	  -e AWS_SECRET_ACCESS_KEY \
	  -e AWS_ACCESS_KEY_ID \
	  gitlab.domain:5050/packer/aws-cli:2.2.40 ec2 describe-images --owners 219670896067 --filters 'Name=name,Values=RHEL-8.4*_HVM-*-x86_64-0-Hourly2-GP2' 'Name=state,Values=available' --query 'reverse(sort_by(Images, &CreationDate))[:1].[Name,ImageId,CreationDate]' --output text --region $AWS_DEFAULT_REGION


#	  gitlab.domain:5050/packer/aws-cli:2.2.40  # ec2 describe-images --owners 219670896067 --filters 'Name=name,Values=RHEL-8*_HVM-*-x86_64-0-Hourly2-GP2' 'Name=state,Values=available' --query 'reverse(sort_by(Images, &CreationDate))[:1].ImageId' --output text --region $REGION)

#aws ec2 describe-images \
#  --owners $GOVCLOUD \
#  --query 'sort_by(Images, &CreationDate)[*].[CreationDate,Name,ImageId]' \
#  --filters "Name=name,Values=RHEL-8*" \
#  --region $AWS_DEFAULT_REGION \
#  --output table

