#!/bin/bash

# modify dnf defaults
grep 'max_parallel_downloads' /etc/dnf/dnf.conf || echo 'max_parallel_downloads=10' >> /etc/dnf/dnf.conf
grep 'fastestmirror' /etc/dnf/dnf.conf || echo 'fastestmirror=True' >> /etc/dnf/dnf.conf
grep 'deltarpm' /etc/dnf/dnf.conf || echo 'deltarpm=true' >> /etc/dnf/dnf.conf

# upgrade the operating system
dnf update -y && dnf autoremove -y

# add
dnf -y install aide audit audit-libs bind-utils ca-certificates curl fapolicyd firewalld git jq opensc opensc openscap-utils openssh-server openssl-pkcs11 parted policycoreutils redhat-lsb-core rng-tools rsyslog rsyslog-gnutls scap-security-guide tmux unzip usbguard yum-utils

# local
dnf -y install $(find . -name "*.rpm")

# ansible
dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
dnf -y install ansible

# remove
dnf -y erase abrt abrt-addon-ccpp abrt-addon-kerneloops abrt-addon-python abrt-cli abrt-plugin-logger abrt-plugin-rhtsupport abrt-plugin-sosreport iprutils krb5-workstation rsh-server sendmail telnet-server tftp-server tuned vsftpd

echo
echo "Package Maintenance Complete"
