#!/usr/bin/env bash

set -o errexit
set -o pipefail

# Generate ansible playbook
oscap xccdf generate fix --fix-type ansible \
  --fetch-remote-resources \
  --profile xccdf_org.ssgproject.content_profile_stig \
  --output stig-role.yml \
  /usr/share/xml/scap/ssg/content/ssg-rhel8-ds.xml
chmod 644 stig-role.yml

# Remediate Host
ansible-playbook \
  -b \
  -f 20 \
  --connection=local -i '127.0.0.1,' \
  --skip-tags 'sudo_remove_nopasswd' stig-role.yml | tee /tmp/.ansible.log

echo "Oscap Stig Complete"
